from dagpost import *
import numpy as np
import scipy
import scipy.stats
import variable as bnvar
import networkx as nx
import bayesian_network as bn
from random import randint
import random 
import scipy.special as ss
import scipy.misc as sc
import matplotlib.pyplot as plt
import ploting
import os
import help_functions as hf
import json
import sys
import logging
import pprint as pp

def get_top_k_inds(a, k):
    sorted_inds = a.argsort()[-len(a):][::-1]
    max_inds = [a.argmax()]

    for j in range(len(a)):
        if len(max_inds) == k: break
        if j > 0:               
           if a[sorted_inds[j]] < a[sorted_inds[j-1]]:
            max_inds += [sorted_inds[j]]
    return max_inds

def get_mcmc_results(i,path,d):
    """ Read 
    Args:
        d: the number of the dataset.
        i: chain number.
    """
    dag = np.loadtxt(path+'dataset_'+str(d)+'_mcmc_dags_chain_'+str(i)+'.txt', skiprows=0,dtype=int)
    tmp = np.loadtxt(path+'dataset_'+str(d)+'_mcmc_zs_chain_'+str(i)+'.txt', skiprows=0,dtype=int)
    tmp = tmp.tolist()
    ztmp = [tuple(z) for z in tmp]
    z = ztmp
    tmp = np.loadtxt(path+'dataset_'+str(d)+'_mcmc_cooper_chain_'+str(i)+'.txt', skiprows=0,dtype=float)
    cooper = tmp.tolist()
    tmp = np.loadtxt(path+'dataset_'+str(d)+'_mcmc_priors_chain_'+str(i)+'.txt', skiprows=0,dtype=float)          
    prior = tmp.tolist()
    return (z,cooper,prior,dag)#,cputimes)

true_path = sys.argv[1]+"/" # eg .../paper
mcmc_path = sys.argv[2]+"/"#+"/alpha_mcmc_"+str(alpha_mcmc)+"_alpha_"+str(alpha)+"_beta1_"+str(b1)+"_beta2_"+str(b2)+"/" # e.g. .../paper/paper100 
#save_path = sys.argv[4]+"/"+c+"/analysis/"#+"/alpha_mcmc_"+str(alpha_mcmc)+"_alpha_"+str(alpha)+"_beta1_"+str(b1)+"_beta2_"+str(b2)+"/" # e.g. .../paper/paper100/analysis
save_path = mcmc_path+"/analysis/"#+"/alpha_mcmc_"+str(alpha_mcmc)+"_alpha_"+str(alpha)+"_beta1_"+str(b1)+"_beta2_"+str(b2)+"/" # e.g. .../paper/paper100/analysis
json_data=open(mcmc_path+"/conf.json").read()   

conf = json.loads(json_data)
#c = sys.argv[2]
log_level = int(sys.argv[4])
    
chain_pool_size = conf["chain_pool_size"]
true_dag_file = conf["true_dag_file"]
true_dag_json_file = conf["true_dag_json_file"]
true_z_file = conf["true_z_file"]
true_cpds_file = conf["true_cpds_file"]
no_most_freq_graph_to_plot = conf["no_most_freq_graph_to_plot"]

data_file = conf["data_file"]
data_sets = conf["data_sets"]
b1 = conf["beta_1"]
b2 = conf["beta_2"]
alpha = conf["alpha"]
alpha_mcmc = conf["alpha_mcmc"]
mode = conf["mode"]
support = conf["support"]
prior = conf["parameter_prior"]
dag_prior = conf["dag_prior"]
gamma = conf["gamma"]
chains = conf["chains"]
print mcmc_path+"/conf.json"
network_name = conf["network_name"]

print true_path
data = np.loadtxt(true_path+data_file, skiprows=0,dtype=int)
data_points = len(data)

if not os.path.exists(save_path):
    os.makedirs(save_path)

if len(chains) == 0: 
    chains = range(chain_pool_size)

# Get the true graph
true_z = np.loadtxt(true_path+true_z_file, skiprows=0,dtype=int)
true_bnet = None
if os.path.isfile(true_path+true_dag_file):
    true_bnet = hf.read_bn(true_path+true_dag_file, support * len(true_z))

elif os.path.isfile(true_path+true_dag_json_file):
    true_bnet = hf.read_bn_json(true_path+true_dag_json_file, support)    

true_bnet.set_data(data, prior, gamma)
true_bnet.color_nodes(true_z)
true_bnet.plot(save_path,"true_dag")

tmp = true_bnet.get_numpy_matrix().view(np.ndarray)
tmp.shape = (len(true_z),len(true_z))    
plt.gca().invert_yaxis()
heatmap = plt.pcolor(tmp, cmap=plt.cm.Greys)    
plt.title(network_name)    
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=16)
plt.savefig(save_path+"true_dag_adjmat.eps", format='eps',dpi=1000)
#true_bnet.plot_heat_map(save_path,"true_dag_adjmat.eps",network_name)

# Read files
dag_chains_datasets = []
z_chains_datasets = []
prior_chains_datasets = []
cooper_chains_datasets = []

for d in range(data_sets):
    dags_chains = []
    z_chains = []
    prior_chains = []
    cooper_chains = []
    for i in range(chain_pool_size):
        (zs_chain, likelihoods_chain, priors_chain, dags_chain) = get_mcmc_results(i, mcmc_path, d)
        dags_chains += [dags_chain]
        z_chains += [zs_chain]
        prior_chains += [priors_chain]
        cooper_chains += [likelihoods_chain]

    dag_chains_datasets += [dags_chains]
    z_chains_datasets += [z_chains]
    prior_chains_datasets += [prior_chains]
    cooper_chains_datasets += [cooper_chains]

# Analyze data independently
for d in range(data_sets):
    all_cooper_chains = np.array([item for sublist in cooper_chains_datasets[d] for item in sublist])
    all_prior_chains = np.array([item for sublist in prior_chains_datasets[d] for item in sublist])
    all_dag_chains = np.array([item for sublist in dag_chains_datasets[d] for item in sublist])
    all_z_chains = np.array([item for sublist in z_chains_datasets[d] for item in sublist])
    
#plt.savefig(save_path+"dataset_"+str(d)+"_priors_"+dag_prior+".eps", format='eps',dpi=1000)

# Get max scoring from each chain and make heat map.

top_scoring_tprs = np.zeros(data_sets * chain_pool_size).reshape(data_sets, chain_pool_size)
top_scoring_spcs = np.zeros(data_sets * chain_pool_size).reshape(data_sets, chain_pool_size)

for d in range(data_sets):
    plt.clf()
    plt.hold(True)
    top_dag_chain = []
    for i in range(chain_pool_size):
        #print "Dataset: "+str(d)+ ", chain: "+str(i)
        zs = np.array(z_chains_datasets[d][i])
        likelihoods = cooper_chains_datasets[d][i]
        priors = prior_chains_datasets[d][i]
        dags = dag_chains_datasets[d][i]
        posteriors = np.array(priors) + np.array(likelihoods)
        top_dag_chain.append(dags[np.argmax(posteriors)])
        N = len(zs[0])

        ## Plotting ##
        # Likelihoods
        title = ""
        x = range(len(likelihoods))

        #plt.subplots_adjust(top=0.80)
        plt.subplot(3,2,3)
        plt.title("\n"+title)
        plt.ylabel(r"$\log {P( \mathbf x | G^{(t)} ) }$")
        plt.xlabel(r"$t$")
        plt.plot(x, likelihoods)
        #plt.savefig(save_path+"dataset_"+str(d)+"_chain_"+str(i)+"_likelihoods_"+dag_prior+".eps", format='eps',dpi=1000)
        #plt.clf()
        # Posteriors
        title = ""
        posts = np.array(priors) + np.array(likelihoods)
        x = range(len(posts))
        #plt.subplots_adjust(top=0.80)
        plt.subplot(3,2,5)
        plt.title("\n"+title)
#        plt.ylabel(r"$\log {P(G^{(t)} | \mathbf x) } + c$")
        plt.ylabel(r"$\log {P( \mathbf x | G^{(t)} )  P(G^{(t)} )}$")
        plt.xlabel(r"$t$")
        plt.plot(x, posts)
        #plt.savefig(save_path+"dataset_"+str(d)+"_chain_"+str(i)+"_posts_"+dag_prior+".eps", format='eps',dpi=1000)
        #plt.clf()
        # Priors
        title = ""
        x = range(len(priors))
        #plt.subplots_adjust(top=0.80)
        plt.subplot(3,2,1)        
        plt.title("\n"+title)
        plt.ylabel(r"$\log { P(G^{(t)} ) }$")
        plt.xlabel(r"$t$")
        plt.plot(x, priors)
        #plt.savefig(save_path+"dataset_"+str(d)+"_chain_"+str(i)+"_priors_"+dag_prior+".eps", format='eps',dpi=1000)
        #plt.clf()

    # For each dataset do:
    if dag_prior == "min-hoppe-beta":
        # Plot the benchmark prior for the true dag
        plt.subplot(3,2,1)
        plt.plot(x, [dag_log_density(true_bnet, true_z, alpha, b1, b2) ] * len(priors), color ="k" )       
        # Plot the benchmark likelihood for the true dag
        plt.subplot(3,2,3)
        plt.plot(x, [true_bnet.cooper_likelihood ] * len(priors), color ="k" )
        # Plot the benchmark posterior for the true dags
        plt.subplot(3,2,5)
        plt.plot(x, [true_bnet.cooper_likelihood + dag_log_density(true_bnet, true_z, alpha, b1, b2) ] * len(priors), color ="k" )
    elif dag_prior == "hoppe-beta":
        # Plot the benchmark prior for the true dag
        plt.subplot(3,2,1)  
        plt.plot(x, [dag_z_log_density(true_bnet, true_z, alpha, b1, b2) ] * len(priors), color ="k" )
        # Plot the benchmark likelihood for the true dag
        plt.subplot(3,2,3)
        plt.plot(x, [true_bnet.cooper_likelihood ] * len(priors), color ="k" )
        # Plot the benchmark posterior for the true dag
        plt.subplot(3,2,5)
        plt.plot(x, [true_bnet.cooper_likelihood + dag_z_log_density(true_bnet, true_z, alpha, b1, b2) ] * len(priors), color ="k" )  
    elif dag_prior == "unif":
        # Plot the benchmark prior for the true dag
        plt.subplot(3,2,1)  
        plt.plot(x, [0.0] * len(priors), color ="k" )
        # Plot the benchmark likelihood for the true dag
        plt.subplot(3,2,3)
        plt.plot(x, [true_bnet.cooper_likelihood ] * len(priors), color ="k" )
        # Plot the benchmark posterior for the true dag
        plt.subplot(3,2,5)
        plt.plot(x, [true_bnet.cooper_likelihood ] * len(priors), color ="k" )  


    all_posterior_chains = all_prior_chains + all_cooper_chains 
    no_max_dags = no_most_freq_graph_to_plot
    max_inds = get_top_k_inds(all_posterior_chains, no_max_dags)
    max_dag_vecs = all_dag_chains[max_inds]
    max_zs = all_z_chains[max_inds]
    print "max scoring z "+str(z_str(max_zs[0]))
    max_ks = [float(max(z)+1) for z in max_zs] 
    #print "Number of layers in "+str(no_max_dags)+" top scoring graphs: "+str(max_ks)
    print "Mean number of layers in "+str(no_max_dags)+" top scoring graphs: "+str(np.mean(max_ks))

    ## Heat map
    max_dag_mats = [ np.matrix(dag_vec).reshape(np.sqrt(len(dag_vec)),np.sqrt(len(dag_vec))) for dag_vec in max_dag_vecs]

    print "TPR for "+str(no_max_dags)+" max scoring DAG"
    #top_scoring_tprs[d] = 
    tpr = np.array([hf.tpr(true_bnet.get_numpy_matrix(), m) for m in max_dag_mats])
    print "max, mean and std TPR for "+str(no_max_dags)+" max scoring DAG"
    print tpr.max(),tpr.mean(), tpr.std()
    #print top_scoring_tprs[d]
    print "SPC for max scoring DAG"
    spc = np.array([hf.spc2(true_bnet.get_numpy_matrix(), m) for m in max_dag_mats])
    print "max, mean and std SPC for "+str(no_max_dags)+" max scoring DAG"
    print spc.max(),spc.mean(), spc.std()

    dag_sizes = [m.sum() for m in max_dag_mats]

    #print "Number of edges in "+str(no_max_dags)+" top scoring graphs: "+str(dag_sizes)
    print "Mean number of edges in "+str(no_max_dags)+" top scoring graphs: "+str(np.mean(dag_sizes))

    #print "Empirical sum:\n" +str(sum(max_dag_mats))
    means_graph = sum(max_dag_mats) / float(no_max_dags)
    
    plt.subplot(3,2,2)
    size = means_graph.shape[0]
    column_labels = range(means_graph.shape[0])
    row_labels = range(means_graph.shape[1])
    data = means_graph.view(np.ndarray)
    data.shape = (size,size) 
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rc('font', size=16)
    plt.gca().invert_yaxis()
    heatmap = plt.pcolor(data, cmap=plt.cm.Greys)    
    plt.title("Top "+str(no_max_dags)+" scoring graphs heatmap from "+str(chain_pool_size)+" chains")    

    # plot max scoring dag
    plt.subplot(3,2,4)
    data = max_dag_mats[0].view(np.ndarray)
    data.shape = (size,size)    
    plt.gca().invert_yaxis()
    heatmap = plt.pcolor(data, cmap=plt.cm.Greys)    
    plt.title("Single top scoring graph from "+str(chain_pool_size)+" chains")    

    # Plot true dag
    # plt.subplot(3,2,2)

    # data = true_bnet.get_numpy_matrix().view(np.ndarray)
    # data.shape = (size,size)    
    # plt.gca().invert_yaxis()
    # heatmap = plt.pcolor(data, cmap=plt.cm.Greys)    
    # plt.title("True graph")    

    # Plot top scoring graph from each chain
    plt.subplot(3,2,6)
    top_chain_dag_mats = [ np.matrix(dag_vec).reshape(np.sqrt(len(dag_vec)),np.sqrt(len(dag_vec))) for dag_vec in top_dag_chain]
    top_chain_means_graph = sum(top_chain_dag_mats) / float(chain_pool_size)
    mat = top_chain_means_graph.view(np.ndarray)
    mat.shape = (size,size) 
    plt.gca().invert_yaxis()
    heatmap = plt.pcolor(mat, cmap=plt.cm.Greys)    
    plt.title("Top scoring graphs heatmap from "+str(chain_pool_size)+" chains")    

    if dag_prior == "min-hoppe-beta" or dag_prior == "hoppe-beta":
        plt.suptitle("Prior: "+dag_prior+", data points: " +str(data_points)+ r", $\alpha =$"+str(alpha) + r", $\beta_{1;i,i+1}=$"+str(b1[0][1])+ r", $\beta_{2;i,i+1}=$"+str(b2[0][1])  + r", $\beta_{1;i,j>i+1}=$"+str(b1[0][2])+ r", $\beta_{2;i,j>i+1}=$"+str(b2[0][2]) + r", $\widetilde{\alpha}=$"+str(alpha_mcmc))
    else:
        plt.suptitle("Prior: "+dag_prior+", data points: " +str(data_points)+ r", $\widetilde{\alpha}=$"+str(alpha_mcmc))
    plt.tight_layout()
    plt.subplots_adjust(top=0.90)
    plt.savefig(save_path+"dataset_"+str(d)+"_prior_"+dag_prior+"_results.eps", format='eps',dpi=1000)

with open(str(save_path)+"/conf.json", 'w') as outfile:
     json.dump(conf, outfile, indent=4, separators=(',', ': '))

