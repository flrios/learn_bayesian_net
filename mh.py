from dagpost import *
import numpy as np
import os
import json
import sys


# Properties
json_data = open(sys.argv[1]).read()
conf = json.loads(json_data)
c = sys.argv[2]

data_path = sys.argv[4]
log_level = int(sys.argv[5])

# Read conf
print "Running conf: "+str(c)

random_seed = conf[c]["random_seed"]
random.seed(random_seed)  # 1
np.random.seed(random_seed)  # 1

mode = conf[c]["mode"]
data_file = conf[c]["data_file"]
data_sets = conf[c]["data_sets"]
T = conf[c]["iterations"]
chain_pool_size = conf[c]["chain_pool_size"]
beta_1 = conf[c]["beta_1"]
beta_2 = conf[c]["beta_2"]
alpha = conf[c]["alpha"]
alpha_mcmc = conf[c]["alpha_mcmc"]
support = conf[c]["support"]
prior = conf[c]["parameter_prior"]
dag_prior = conf[c]["dag_prior"]
gamma = conf[c]["gamma"]

time = str(strftime("%Y-%m-%d_%H%M%S", gmtime()))
path = sys.argv[3]+"/"+time+"/"

# path = sys.argv[3]+"/"+c+"/alpha_mcmc_"+str(alpha_mcmc)+"_alpha_"
# +str(alpha)+"_beta1ii+1_"+str(beta_1[0][1])+"_beta2ii+1_"+str(beta_2[0][1])+"/"

if not os.path.exists(path):
    os.makedirs(path)

logging.basicConfig(filename=path+'/mcmc.log', level=log_level)

data = np.loadtxt(data_path+data_file, skiprows=0, dtype=int)
n = len(data[0])

if len(support) == 1:
    support = support*n

# Stochastic search
# Start with empty graph
for d in range(data_sets):
    for j in range(chain_pool_size):
        nodes = [bnvar.Variable(i, support[i]) for i in range(n)]
        network = bn.BayesianNetwork()
        network.set_nodes(nodes)
        network.set_data(data, prior, gamma)
        z = np.zeros(n, dtype=int)
        nets = []
        zs = []
        dags = []
        likelihoods = []
        priors = []
        accepted = 0.0
        for r in range(1, T):
            print "dataset: "+str(d) + "/" + str(data_sets-1)
            print ", chain: "+str(j) + "/" + str(chain_pool_size-1)
            print ", sim: " + str(r) + "/" + str(T)
            print "\nCurrent"
            print z_str(z)
            (z2, network2) = prop3(z, network, alpha_mcmc)
            prior1 = 0
            prior2 = 0
            if dag_prior == "min-hoppe-beta":
                prior1 = dag_log_density(network, z, alpha, beta_1, beta_2)
                prior2 = dag_log_density(network2, z2, alpha, beta_1, beta_2)
            if dag_prior == "hoppe-beta":
                prior1 = dag_z_log_density(network, z, alpha, beta_1, beta_2)
                prior2 = dag_z_log_density(network2, z2, alpha, beta_1, beta_2)

            ll1 = network.cooper_likelihood
            ll2 = network2.cooper_likelihood
            log_post_1 = prior1 + ll1
            log_post_2 = prior2 + ll2
            ratio = 1.0
            accept_prob = min(1.0, np.exp(log_post_2-log_post_1) * ratio)
            print "accept_prob: "+str(accept_prob)
            accept = np.random.binomial(1, accept_prob)
            if accept:
                accepted += 1
                # print "ACCEPT"
                zs += [z2]
                dags += [np.array(network2.get_numpy_matrix().flatten())[0]]
                likelihoods += [network2.cooper_likelihood]
                priors += [prior2]
                # Update
                network = network2
                z = z2
            else:
                zs += [z]
                dags += [np.array(network.get_numpy_matrix().flatten())[0]]
                likelihoods += [network.cooper_likelihood]
                priors += [prior1]
        np.savetxt(path+'/dataset_'+str(d)+'_mcmc_dags_chain_' +
                   str(j)+'.txt', dags, fmt='%.1i')
        np.savetxt(path+'/dataset_'+str(d)+'_mcmc_zs_chain_' +
                   str(j)+'.txt', zs, fmt='%.1i')
        np.savetxt(path+'/dataset_'+str(d)+'_mcmc_cooper_chain_' +
                   str(j)+'.txt', likelihoods, fmt='%10.15f')
        np.savetxt(path+'/dataset_'+str(d)+'_mcmc_priors_chain_' +
                   str(j)+'.txt', priors, fmt='%10.15f')

with open(str(path)+'/conf.json', 'w') as outfile:
    json.dump(conf[c], outfile, indent=4, separators=(',', ': '))
