# virtualenv minmiljo
# Use this to setup env
# http://iamzed.com/2009/05/07/a-primer-on-virtualenv/
# cd minmiljo
# source bin/activate

easy_install pip
easy_install numpy
easy_install scipy
easy_install networkx

brew install graphviz
brew install pkg-config
easy_install pygraphviz

easy_install pebl
## Use this to install pydot 
## http://answers.ros.org/question/64133/cant-install-pydot-on-macbook-pro-os-1083/