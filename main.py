from chinese_rest import *
import json
import sys
import os
import help_functions as hf
import numpy as np

# Properties
json_data = open(sys.argv[1]).read()
conf = json.loads(json_data)
c = sys.argv[2]

# Global conf
print "Running conf: "+str(c)
random_seed = conf[c]["random_seed"]

path = conf[c]["path"]
if not os.path.exists(path):
    os.makedirs(path)

# Partition conf
n = conf[c]["n"]

# Network conf
data_simulations = conf[c]["data_simulations"]
data_sets = conf[c]["data_sets"]
gamma = conf[c]["gamma"]
support = conf[c]["support"]
mode = conf[c]["mode"]
if len(support) == 1:
    support = support*n

cpds_file = conf[c]["cpds_file"]
dag_file = conf[c]["dag_file"]
dag_json_file = conf[c]["dag_json_file"]
data_file = conf[c]["data_file"]

np.random.seed(random_seed)

z = None
dag = None
cpds = None
data = None
bnet = None
# Want to simulate data or structures
if os.path.isfile(path+dag_file):
    dag = hf.read_networks(path+dag_file)
    bnet = hf.read_bn(path+dag_file, support)
else:
    pass

if os.path.isfile(path+dag_json_file):
    bnet = hf.read_bn_json(path+dag_json_file, support)
else:
    pass

if os.path.isfile(path+cpds_file):
    print "Reading cpds from "+path+cpds_file
    read_cpds(path+cpds_file, bnet)
else:
    print "Generating new cpds with gamma = "+str(gamma)
    bnet.generate_cpds(gamma)
    bnet.cpds_tofile(path+cpds_file)

for i in range(data_sets):
    print "Simulating " + str(data_simulations) + " data set"
    data = bnet.simulate(data_simulations)
    np.savetxt(path+str(i)+"_"+data_file, data, fmt='%.1i')
    print "Saved data to " + path+str(i)+"_"+data_file
