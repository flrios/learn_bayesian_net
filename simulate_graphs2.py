import random
import crp 
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from pygraphviz import *
import pygraphviz
from sets import Set
from help_functions import *
import os
import json 
import shutil
import operator 
import sys
import pprint as pp
import pylab as pl
from dagpost import *
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def expectation(alpha, N):
    """ Expected number of cells from hoppes urn 
    """
    e = 0
    for i in range(N):
        e += np.float(alpha)/np.float(alpha+i)
    return e

def generate_rho(z):
    K = max(z)+1
    r = range(K)
    random.shuffle(r)
    return r

def generate_sigma(N):    
    r = range(N)
    random.shuffle(r)
    return r

def generate_z(N,alpha):
    z = np.zeros(N,dtype=int)
    a = [1.0]

    for i in range(1,N):
        dist = np.array(a + [alpha]) / float(i + alpha )
        samp = np.random.multinomial(1,dist) 
        t = np.where(samp==1)[0][0]
        z[i] = t
        if t < len(a):
            a[t] += 1.0
        else:
            a += [1.0]
    return z

def generate_dag(z,rho,sigma, xi):
    """ z is already adjusted for sigma 
    """
    N = len(z)
    dag = nx.DiGraph()
    colors = ['red','green','black','blue','cyan','yellow','orange','brown'] * (max(z)+1)

    for i in range(N): 
        dag.add_node(i, color=colors[z[i]])

    # As long as we do not use different distributions for the probability of
    # an edges between classes we can use i och sigma[i].
    for i in range(N):
        for j in range(N):
            # No need to check if group is above rho since it is already embedded in xi.
            # if rho[z[i]] < rho[z[j]]:
            # print xi[z[i]][z[j]]
            # e = bernoulli.rvs(xi[z[i]][z[j]])
            e = bernoulli.rvs(xi[i][j])
            if e == 1:
                dag.add_edge(i,j)
    return dag

def generate_dag_2(z,rho,sigma, xi):
    """ z is already adjusted for sigma.
        Generates a DAG where z is aits minimal layering. 
    """
    N = len(z)
    dag = nx.DiGraph()
    colors = ['red','green','black','blue','cyan','yellow','orange','brown'] * (max(z)+1)

    for i in range(N): 
        dag.add_node(i, color=colors[z[i]])

    edges = add_required_edges(dag, z, rho)
    # As long as we do not use different distributions for the probability of
    # an edges between classes we can use i och sigma[i].
    for i in range(N):
        for j in range(N):
            # No need to check if group is above rho since it is already embedded in xi.
            # if rho[z[i]] < rho[z[j]]:
            # print xi[z[i]][z[j]]
            # e = bernoulli.rvs(xi[z[i]][z[j]])
            e = bernoulli.rvs(xi[i][j])
            if e == 1 and not e in edges:
                dag.add_edge(i,j)
    return dag

def add_required_edges(dag, z, rho):
    """ Adds one parent in layer above for every node.
    """
    N = len(z)
    K = max(z)+1
    edges = []

    z_rho = [rho[s] for s in z] # use shuffeld tables
    for n in xrange(0, N):
        if z_rho[n] == 0: continue
        poss_parents = nodes_in_tables( [ z_rho[n] - 1] , z_rho )
        dist = [1/float(len(poss_parents))] * len(poss_parents)
        samp = np.random.multinomial(1,dist) 
        i = np.where(samp==1)[0][0]
        parent = poss_parents[i]
        edges += [(parent, n)]

    dag.add_edges_from(edges)
    return edges

def generate_xi(beta,z,rho,mode):
        """ Creates a matrix of edge probababilities. 
            There are different versions here, depending on how we choose the prior.

            1. (all)    Each edge has its own edge probability (but eq distributed),
                        we restrict it to beta1 and beta2.
            2. (single) There is only one edge probability.
            3. (group)  Each pair of classes has their own edge probability.
            4. (group NOT IMPLEMENTED)  Each pair of classes has equally distributed edge probability.
        """
        N = len(z)
        K = max(z)+1
        
        xi = np.zeros(N*N).reshape(N,N)
        nu = np.zeros(K*K).reshape(K,K)
        if mode == "single":
            xi += random.betavariate(beta[0][0], beta[1][0])

        if mode == "all":
            for i in range(N) :
                for j in range(i+1,N) :
                    xi[i][j] = random.betavariate(beta[0][0], beta[1][0])

        if mode == "group":
            for i in range(K) :
                for j in range(i+1,K) :
                    #xi[i][j] = random.betavariate(beta[0][0], beta[1][0])                    
                    nu[i][j] = random.betavariate(beta[0][0], beta[1][0])

            for i in range(N):
                for j in range(N):
                    if rho[z[i]] < rho[z[j]]:
                        xi[i][j] = nu[z[i]][z[j]]
            #print z
            #print nu
            #print xi

        if mode == "indepcause":
            a = random.betavariate(beta[0][0], beta[1][0])            
            for i in range(K) :
                xi[i][i+1] = a
        return xi

print sys.argv[1]
json_data=open(sys.argv[1]).read()    
conf = json.loads(json_data)

results = {"simres":{}}

c = sys.argv[2]
# Global conf
print "Running conf: "+str(c)
path = conf[c]["path"]
plots = conf[c]["plots"]
random_seed = conf[c]["random_seed"]
no_persons = conf[c]["no_persons"]
no_restaurants = conf[c]["no_restaurants"]
beta = conf[c]["network"]["beta"]
alpha = conf[c]["restaurant"]["alpha"]

mode = conf[c]["network"]["mode"]

print beta
# Set random seed 
random.seed(random_seed)
np.random.seed(random_seed)
state = random.getstate()
random.setstate(state)

dags = [ [ [] for i in range(len(alpha))] for j in range(len(no_persons)) ]
zs = [ [ [] for i in range(len(alpha))] for j in range(len(no_persons)) ]

graph_density = [] # function of N
if not os.path.exists(path):
    os.makedirs(path)

# Simulate dags
for s in range(len(no_persons)) :
    N = no_persons[s]
    for l in range(len(alpha)) :
        for i in range(no_restaurants) :
            # Generate DAG
            #sigma = generate_sigma(N)
            sigma = range(N)
            z_no_shuffle = generate_z(N, alpha[l])
            z = [ z_no_shuffle[j] for j in sigma]
            #rho = generate_rho(z)
            rho = range(max(z)+1)
            xi = generate_xi(beta,z,rho,mode)                                
            dag = generate_dag_2(z,rho,sigma,xi)

            dags[s][l].append(dag)           
            zs[s][l].append(z)

# plot
for s in range(len(no_persons)) :
    N = no_persons[s]
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rc('font', size=20)
    #plt.subplots_adjust(top=0.8)
    #pl.subplots_adjust( hspace=1.5 )    
    #plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=1.0)    
    plt.subplots_adjust(hspace=0.5)

    #plt.suptitle(r'$T= $'+str(no_restaurants)+r",$ N= $"+str(N)+r", $\beta_1=$ "+str(beta[0][0])+r", $\beta_2=$ "+str(beta[1][0]))
    for l in range(len(alpha)) :
        res = {}

        diameter = np.zeros(N)
        edges = np.zeros(N*N)
        classes = np.zeros(N*N).reshape(N,N)
        no_classes = np.zeros(N+1)
        expected_classes = 0
        no_immoralities = np.zeros(N*N*N) # NOT same as v-structures

        for i in range(no_restaurants) :
            dag = dags[s][l][i]
            z = zs[s][l][i]

            if i < plots :
                # Plot three graphs
                agraph = nx.to_agraph(dag)

                N = len(z)
                K = max(z) + 1
                tables = [[] for r in range(K)]

                for r in range(K):
                    for j,k in enumerate(z):
                        if k == r : tables[r].append(j)
                for r,t in enumerate(tables):
                    agraph.add_subgraph(t, rank='same')

                agraph.node_attr['shape']='circle'
                agraph.node_attr['size']=10
                agraph.draw(str(path)+"/dag_N"+str(N)+"_"+str(i)+"alpha"+str(alpha[l])+"_sim"+str(i)+".eps",prog='dot')
                agraph.write(str(path)+"/dag_N"+str(N)+"_"+str(i)+"alpha"+str(alpha[l])+"_sim"+str(i)+".dot")
            # Compute features
            
            #diameter[dag_diameter(dag)] += 1
            no_immoralities[len(get_immoralities(dag))] += 1
            edges[nx.number_of_edges(dag)] += 1

            no_classes[max(z)+1] += 1
            if i == 0 :
                expected_classes = expectation(alpha[l], N)
                
        total_no_edges = np.float(sum([i*j for i,j in enumerate(edges)]))
        total_no_immoralities = np.float(sum([i*j for i,j in enumerate(no_immoralities)]))
        
        graph_density.append(total_no_edges / np.float(N*no_restaurants))

        res["N"] = N
        res["alpha"] = alpha[l]
        res["beta"] = beta
        res["true E[#classes]"] = expected_classes    
        res["empirical E[#edges]"] =  np.float(total_no_edges)/np.float(no_restaurants)
        res["empirical E[#immoralities]"] =  np.float(total_no_immoralities)/np.float(no_restaurants)
        res["graph density"] = total_no_edges / np.float(N*no_restaurants)
        res["Distribution of #edges"] = dict( (k,(edges/no_restaurants)[k]) for k in range(len(edges)) if not sum(edges[k:]) == 0.0 )
        res["Distribution of #classes"] = dict( (k,(no_classes/no_restaurants)[k]) for k in range(len(no_classes)) if not sum(no_classes[k:]) == 0.0)
        res["Distribution of #immoralities"] = dict( (k,(no_immoralities/no_restaurants)[k]) for k in range(len(no_immoralities)) if not sum(no_immoralities[k:]) == 0.0)

        #results["simres"][N][alpha[l]] = res 
        results["simres"]["N:"+str(N)+" alpha:"+str(alpha[l])] = res 

        #plt.subplot(len(alpha)*2, 2, l*2+1)
        plt.subplot(len(alpha), 1, l+1)
        plt.bar(range(len(edges)),edges/no_restaurants)
        #plt.xlabel(r"$\phi_E(\mathcal G)=$ 'Number of edges'. $\alpha=$ "+str(alpha[l])+r".")
        plt.xlabel(r"$\alpha=$ "+str(alpha[l]) + r", $\beta_1 = $" + str(beta[0][0]) + r", $\beta_2 = $" + str(beta[1][0]))
        plt.ylabel(r"$\hat P(\phi_E(D))$")

        #plt.subplot(len(alpha)*2,2,l*2+2)        
        #plt.bar(range(len(no_classes)),no_classes/no_restaurants)
        #plt.xlabel(r"Number of classes"+r". $\alpha=$ "+str(alpha[l])+r".")
        #plt.ylabel(r"Estimated probability")

        # plt.subplot(313)
        # plt.bar(range(len(no_immoralities)),no_immoralities/no_restaurants)
        # plt.xlabel(r"$\phi_I(\mathcal G)=$ 'Number of immoralities'. $\alpha=$ "+str(alpha[l])+r", $\beta_1=$ "+str(beta[0][0])+r", $\beta_2=$ "+str(beta[1][0])+r".")
        # plt.ylabel(r"$\hat P[\phi_E(\mathcal G)]$")    

        # plt.tight_layout()
    plt.savefig(str(path)+"/edgesN"+str(N)+"b1"+str(beta[0][0])+"b2"+str(beta[1][0])+"alpha"+str(alpha)+".eps", format='eps',dpi=1000)


#print "Edges density indexed by "

pp.pprint(results)

results["conf"] = conf[c]
with open(str(path)+"/summary_N"+str(N)+"b1"+str(beta[0][0])+"b2"+str(beta[1][0])+"alpha"+str(alpha)+'.json', 'w') as outfile:
     json.dump(results, outfile, indent=4, separators=(',', ': '))
            
# plt.subplot(313)
# plt.bar(range(len(edges)),[ v/np.float(N) for v in edges])
# plt.bar(range(len(no_classes)),no_classes/no_restaurants)
# plt.title("Edge density, N:"+str(N))
     
    #print(len(no_classes))
