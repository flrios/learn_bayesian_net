##FLAGS=-3 -d
FLAGS=-d

all:
	python $(FLAGS) chinese_rest.py 

simulate:
	python $(FLAGS) simulate_graphs.py 

mcmc:
	python $(FLAGS) mcmc.py

clean:
	rm -f *.png *.pyc .DS_Store octave-core
