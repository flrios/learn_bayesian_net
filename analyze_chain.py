import json
import sys
import logging
import numpy as np
import mcmc
import analyze_mcmc
import matplotlib.pyplot as plt
import networkx as nx
import help_functions as hf
from rest_graph import *
import os
from collections import Counter
import variable as bnvar
import ploting

json_data=open(sys.argv[1]).read()
conf = json.loads(json_data)
c = sys.argv[2]

true_path = sys.argv[3]+"/" # eg .../paper
mcmc_path = true_path+"/"+c+"/" # e.g. .../paper/paper100
save_path = sys.argv[4]+"/"+c+"/analysis/" # e.g. .../paper/paper100/analysis
log_level = int(sys.argv[5])

chain_pool_size = conf[c]["chain_pool_size"]
true_dag_file = conf[c]["true_dag_file"]
true_dag_json_file = conf[c]["true_dag_json_file"]

true_z_file = conf[c]["true_z_file"]
true_cpds_file = conf[c]["true_cpds_file"]
no_most_freq_graph_to_plot = conf[c]["no_most_freq_graph_to_plot"]

data_file = conf[c]["data_file"]
data_sets = conf[c]["data_sets"]
b1 = conf[c]["beta_1"]
b2 = conf[c]["beta_2"]
alpha = conf[c]["alpha"]
mode = conf[c]["mode"]
support = conf[c]["support"]
prior = conf[c]["parameter_prior"]
gamma = conf[c]["gamma"]

chains = conf[c]["chains"]
conf = conf[c]

if not os.path.exists(save_path):
    os.makedirs(save_path)

if len(chains) == 0: chains = range(chain_pool_size)

(zs,cooper,prob,dags,cputimes) = analyze_mcmc.get_mcmc_results(chains, mcmc_path)
n = len(dags[0][0])
if len(support) == 1: support = support*n

# Getting the true score under the assumption of z
true_bnet = None

if os.path.isfile(true_path+true_dag_file):
    dag = hf.read_networks(true_path+true_dag_file)
    true_bnet = hf.read_bn(true_path+true_dag_file, support)

elif os.path.isfile(true_path+true_dag_json_file):
    true_bnet = hf.read_bn_json(true_path+true_dag_json_file, support)

#hf.read_cpds(true_path+true_cpds_file, true_bnet)
data = np.loadtxt(true_path+data_file, skiprows=0,dtype=int)
true_bnet.set_data(data,prior,gamma)
true_rest = hf.read_z(true_path+true_z_file, alpha)
true_rg = RestGraph(true_rest, b1, b2, mode)
true_rg.set_network(true_bnet)

true_bnet.plot(save_path,"true_dag")
true_bnet.plot_heat_map(save_path,"true_dag_adjmat.eps","")

###### Plot scores #####
true_score = 0
title = ""
x = range(len(prob[0]))
plt.subplots_adjust(top=0.80)
plt.title("\n"+title)
plt.ylabel(r"$\log {P(d^{(t)}, \underline z^{(t)} | \mathbf x)}$")
plt.xlabel(r"$t$")
for scores in  prob :
    plt.plot(x, scores)

if os.path.isfile(true_path+true_cpds_file):
    true_scores = [true_rg.prob()]*len(x)
    plt.plot(x,true_scores)

plt.savefig(save_path+"scores.eps", format='eps',dpi=1000)


# Get all stats in one array
(all_probs,all_cooper,all_dags,
 all_zs,all_cpu_times) = analyze_mcmc.merge_chains(zs,cooper,
                                                   prob,dags,cputimes)

# Create bayesian networks
all_bnets = []
for i in range(len(all_probs)):

    nodes = [bnvar.Variable(j, support[j]) for j in range(n)]
    bnet = bn.BayesianNetwork()
    bnet.set_nodes(nodes)

    for edge in all_dags[i].edges():
        bnet.add_edge(edge)

    bnet.color_nodes(all_zs[i])
    all_bnets.append(bnet)

# marginalize over DAGs
bncounter = Counter()
for g in all_bnets:
	bncounter[g] += 1

for i, (bnet,freq) in  enumerate(Counter(bncounter).most_common(2)):
	bnet.plot(save_path,"marginal_dag_"+str(i) +"_freq_"+str(freq))
	bnet.plot_heat_map(save_path,"marginal_heatmap_"+str(i) +"_freq_"+str(freq),"")

results = {"mcmcres": {}}

results["mcmcres"]["cpu_times"] = all_cpu_times
results["mcmcres"]["avg_cpu_times"] = sum(all_cpu_times)/len(all_cpu_times)
results["mcmcres"]["high_score"] = []

true_dag = true_bnet.dag
sort_by_prob_index = (-all_probs).argsort()

# plot best
all_bnets[sort_by_prob_index[0]].plot(save_path, "best_dag_partition")

i = 0
tpr = hf.tpr(nx.to_numpy_matrix(true_dag), nx.to_numpy_matrix(all_dags[i]))
spc1 = hf.spc1(nx.to_numpy_matrix(true_dag), nx.to_numpy_matrix(all_dags[i]))
spc2 = hf.spc2(nx.to_numpy_matrix(true_dag), nx.to_numpy_matrix(all_dags[i]))
results["mcmcres"]["high_score"].append({"log prob": all_probs[i],
                                         "tpr": tpr,
                                         "spc1": spc1,
                                         "spc2": spc2})

# Write to file
# Include score to the plots
with open(str(save_path)+'/mcmc_results.json', 'w') as outfile:
    json.dump(results, outfile, indent=4, separators=(',', ': '))

# To latex
# np.savetxt("mydata.csv", a, delimiter=' & ', fmt='%2.2e', newline=' \\\\\n')

# Heat map
dag_mats = [nx.to_numpy_matrix(d) for d in all_dags[conf["burnin"]:]]
means_graph = sum(dag_mats) / len(all_dags[conf["burnin"]:])
ploting.plot_heat_map(means_graph, save_path,
                      "edge_heatmap",
                      "Edge empirical marginal probabilities\n\n")

means_partition = np.zeros((n, n), dtype=float)
for i in range(len(all_zs[conf["burnin"]:])):
    z = all_zs[i]
    for j in range(n):
        for k in range(n):
            if z[j] == z[k]:
                means_partition[j][k] += 1.0
                means_partition[k][j] += 1.0

means_partition /= len(all_zs[conf["burnin"]:])

ploting.plot_heat_map(means_partition,save_path,
              "partition_heatmap",
              "Same tables empirical probabilities\n\n")

with open(str(save_path)+'/settings.json', 'w') as outfile:
  json.dump(conf, outfile, indent=4, separators=(',', ': '))
