import unittest
#from mcmc import  *
import numpy as np
import bayesian_network as bn
import variable as bnvar
import networkx as nx
from ploting import *
import pprint as pp
# THIS IS ESSENTIAL FOR THE TESTS
#random.seed(1)
#np.random.seed(1)
#state = random.getstate()
#random.setstate(state)

class TestLikelihood(unittest.TestCase):
    def setUp(self):
        self.gamma = 1
    
    def test_count_parent_conf_nopar(self):
        nd = bnvar.Variable(0,2) ## id = 0 range = 2
        data = np.array([[0],
                         [0]])
        g = bn.BayesianNetwork()
        g.set_nodes([nd])
        g.set_data(data)

        parent_conf=()        
        g.count_node(data,nd)
        n = g.count_parent_conf(nd,parent_conf)
        self.assertEqual(n,2)

    def test_count_parent_conf(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]
        data = np.array([[0,1,1],
                         [0,1,0]])

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,2),(1,2)])

        g.count_node(data,nodes[2])
        parent_conf=(0,1)

        n = g.count_parent_conf(nodes[2],parent_conf)
        self.assertEqual(n,2)

        parent_conf=(1,1)
        n = g.count_parent_conf(nodes[2],parent_conf)
        self.assertEqual(n,0)
    
    def test_count_parent_node_conf(self):
        nodes = [bnvar.Variable(i,2) for i in range(2)]        
        data = np.array([[0,1],
                         [0,1],
                         [1,0],
                         [1,1],
                         [0,0]])

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1)])

        g.count_node(data,nodes[1])
        
        parent_conf= tuple([0])
        node_conf = 1
        n = g.count_parent_node_conf(nodes[1], node_conf, parent_conf)
        self.assertEqual(n,2)
        parent_conf = tuple([1])
        node_conf = 1
        n = g.count_parent_node_conf(nodes[1],node_conf,parent_conf)
        self.assertEqual(n,1)

    def test_calc_fam_score_noparent(self):
        nodes = [bnvar.Variable(i,2) for i in range(1)]
        data = np.array([[1]])
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)

        gamma = 1

        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score,np.log(0.5) )) 
        data = np.array([[1],[1]])
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score, np.log(1.0/3) ) )
        data = np.array([[1],[1],[0]])
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score,  np.log(1.0/12)  ) )
        data = np.array([[0],[1]])
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score,  np.log(1.0/6) ) ) 
        data = np.array([[1],[0],[0],[1],[1]])
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score,  np.log(12.0/720)  ) )

    def test_calc_fam_score(self):
        gamma = 1
        nodes = [bnvar.Variable(i,2) for i in range(2)]
        data = np.array([[1,1]])
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1)])

        # 1
        
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score, np.log(0.5) )) 
        data = np.array([[1,1],
                         [1,0]])
        g.set_data(data)
        score = g.calc_fam_score(nodes[1],data,gamma)        
        self.assertTrue( np.allclose(score, np.log(1.0/6) )) 
        data = np.array([[1,1],
                         [1,0],
                         [0,1]])
        g.set_data(data)
        score = g.calc_fam_score(nodes[1],data,gamma)
        self.assertTrue( np.allclose(score, np.log(1.0/12) )) 

        nodes = [bnvar.Variable(i,2) for i in range(3)]
        data = np.array([[1,1,1],
                         [1,0,1],
                         [0,1,0]])
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(2,1)])
        
        score = g.calc_fam_score(nodes[0],data,gamma)
        self.assertTrue( np.allclose(score, np.log(1.0/12) )) 

    def test_count_node_diamond(self):
        gamma = 1
        nodes = [bnvar.Variable(i,2) for i in range(4)]
        nodes[0].cpd = np.array([0.2, 0.8])
        nodes[1].cpd = np.array([ [0.2, 0.8], 
                                  [0.4, 0.6] ])        
        nodes[2].cpd = np.array([ [0.3, 0.7], 
                                  [0.7, 0.3] ])
        nodes[3].cpd = np.array([[ [0.3, 0.7], 
                                   [0.7, 0.3] ],
                                 [ [0.6, 0.4], 
                                   [0.8, 0.2] ]])
        
        data = np.array([[0,0,0,0],
                         [0,1,1,0],
                         [1,1,0,1]])

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(0,2),(1,3),(2,3)])
        
        g.count_node(data, nodes[0])
        #pp.pprint (nodes[0].counts)

        g.count_node(data, nodes[1])
        #pp.pprint (nodes[1].counts)
        
        g.count_node(data, nodes[2])
        #pp.pprint (nodes[2].counts)
        
        g.count_node(data, nodes[3])
        #pp.pprint (nodes[3].counts)

    def test_cooper_herzkovitz_1(self):
        nodes = [bnvar.Variable(i,2) for i in range(2)]        
        data = np.array([[1,1],
                         [1,0],
                         [0,1]])
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1)])

        score = g.cooper_herzkovitz(data,1)
        self.assertTrue( np.allclose(score, np.log(1.0/(12*12))  ))

    # Check so that Markov equivalent graphs has the same score. 
    # This need not be the case so we skip.
    #@unittest.skip("not correct yet")
    def test_cooper_herzkovitz_diamond(self):
        nodes = [bnvar.Variable(i,2) for i in range(4)]        
        
        data = np.array([[0,0,0,0]])


        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        nodes[0].cpd = np.array([0.2, 0.8])
        nodes[1].cpd = np.array([ [0.2, 0.8], 
                                  [0.4, 0.6] ])        
        nodes[2].cpd = np.array([ [0.3, 0.7], 
                                  [0.7, 0.3] ])
        nodes[3].cpd = np.array([[ [0.3, 0.7], 
                                   [0.7, 0.3] ],
                                 [ [0.6, 0.4], 
                                   [0.8, 0.2] ]])
        g.set_data(data)
        g.add_edges([(0,1),(0,2),(1,3),(2,3)])

        score = g.cooper_herzkovitz(data,1)        
        correct = -2.77258872224
        self.assertAlmostEqual( score, correct  )
    
        data = np.array([[0,0,0,0],
                         [0,1,1,0],
                         [1,1,0,1]])

        correct = -9.53416149104
        score = g.cooper_herzkovitz(data,1)
        self.assertAlmostEqual( score, correct  )        

    @unittest.skip("Not applicable.")
    def test_cooper_herzkovitz_same_score(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)] 
        
        sim_g = bn.BayesianNetwork()
        sim_g.set_nodes(nodes)        
        sim_g.add_edges([(0,1),(1,2)])

        p = 0.2
        # Set cpd
        nodes[0].cpd = np.array([0.5, 0.5])
        nodes[1].cpd = np.array([ [0.5-p, 0.5+p], [0.5+p, 0.5-p] ])
        nodes[2].cpd = np.array([ [0.5+p, 0.5-p], [0.5-p, 0.5+p] ])

        n=3000
        data = sim_g.simulate(n)

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(1,2)])

        score_chain = g.cooper_herzkovitz(data,1)
        print g.cooper_likelihood/n
        g.del_edges([(1,2)])
        g.add_edges([(2,1)])
        score_fork = g.cooper_herzkovitz(data,1)        
        print score_chain/n,score_fork/n,score_chain > score_fork

        print g.cooper_likelihood/n
        self.assertEqual(score_chain , score_fork  ) 

    def test_cooper_herzkovitz_2(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]
        data = np.array([[1,1,1],
                         [1,0,1],
                         [0,1,0]])
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(2,1)]) # v-structure

        score = g.cooper_herzkovitz(data,1)
        s0  = g.calc_fam_score(nodes[0],data)
        s1  = g.calc_fam_score(nodes[1],data)
        s2  = g.calc_fam_score(nodes[2],data)
       
        exp = s0+s1+s2
        #print exp

        self.assertTrue( np.allclose(score, exp ))
        
    def test_add_edge(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]        
        nodes2 = [bnvar.Variable(i,2) for i in range(3)]        
        data = np.array([[1,1,1],
                         [1,0,1],
                         [0,1,0],
                         [0,1,1]])

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edge((0,1))
        g.add_edge((2,1))

        g2 = bn.BayesianNetwork()
        g2.set_nodes(nodes2)
        g2.set_data(data)
        g2.add_edges([(0,1), (2,1)])

        self.assertEqual(g.cooper_likelihood, g2.cooper_likelihood)
        
    def test_update_cooper1(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]        
        data = np.array([[1,1,1],
                         [1,0,1],
                         [0,1,0],
                         [0,1,1]])
        
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edge((0,1))
        g.add_edge((2,1))
        

        l = g.cooper_likelihood
        coop = g.cooper_herzkovitz(data)
        coop2 = g.cooper_herzkovitz(data)
        self.assertAlmostEqual(l ,coop)
        self.assertAlmostEqual(g.cooper_likelihood ,coop)
        self.assertAlmostEqual(g.cooper_likelihood ,coop2)

    def test_update_cooper2(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]
        data = np.array([[1,1,1],
                         [1,0,1],
                         [0,1,0],
                         [0,1,1]])

        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(2,1)])

        s1 = g.cooper_likelihood 
        g.del_edges([(2,1)])
        s2 = g.cooper_likelihood 
        g.add_edges([(2,1)])
        s3 = g.cooper_likelihood 
        coop = g.cooper_herzkovitz(data)
        self.assertAlmostEqual(g.cooper_likelihood ,coop)
        self.assertEqual(s1 ,s3)
        #print g.diff_cache.cache

class TestBN(unittest.TestCase):

    def test_simulate(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)        
        g.add_edges([(0,2),(1,2)])
        g.generate_cpds()
        g.simulate(10)

        nodes = [bnvar.Variable(i,3) for i in range(7)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.add_edges([(0,1),(0,2),(2,1),(1,3),(2,3),(2,4),(5,4),(6,2),(6,5)])
        g.generate_cpds()
        g.simulate(20)

    @unittest.skip("No real test.")    
    def test_estimate_cpds(self):
        """
        Testing estimating cpds
        """
        nodes = [bnvar.Variable(i,2) for i in range(4)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.add_edges([(0,1),(0,2),(1,3),(2,3)])
        g.generate_cpds()
        print "True cpds"
        for v in g.get_nodes():
            print v.cpd        
        obs = 10000
        data = g.simulate(obs)
        nodes = [bnvar.Variable(i,2) for i in range(4)]
        g2 = bn.BayesianNetwork()
        g2.set_nodes(nodes)
        g2.add_edges([(0,1),(0,2),(1,3),(2,3)])
        g2.set_data(data)
        g2.estimate_cpds(data)

        print "Estimated cpds based on "+str(obs)+" observations"
        for v in g2.get_nodes():
            print v.cpd     

    @unittest.skip("No real test.")            
    def test_cpd(self):
        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.add_edges([(0,2),(1,2)])

        g.generate_cpds()
        #print g.get_nodes()[2].cpd[1][1]

    @unittest.skip("Not supported in this version.")    
    def test_create_simulation_order(self):
        nodes = [bnvar.Variable(i,2) for i in range(7)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(0,2),(2,1),(1,3),(2,3),(2,4),(5,4),(6,2),(6,5)])

        order =  g.create_simulation_order()
        facit = [0,6,2,1,3,5,4]
        self.assertEqual(order,facit)
        
        nodes = [bnvar.Variable(i,2) for i in range(4)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,1),(0,2),(2,1),(1,3),(2,3)])

        order =  g.create_simulation_order()
        facit = [0,2,1,3]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork()
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges([(0,2),(1,2)])

        order =  g.create_simulation_order()
        facit = [0,1,2]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(4)]
        g = bn.BayesianNetwork(nodes,[(0,1),(0,2),(1,3),(2,3)])        
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [0,1,2,3]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(2)]
        g = bn.BayesianNetwork(nodes,[(1,0)])        
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [1,0]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork(nodes,[(0,1),(1,2)])        
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [0,1,2]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork(nodes,[(1,2),(1,0)])        
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [1,0,2]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(3)]
        g = bn.BayesianNetwork(nodes)
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [0,1,2]
        self.assertEqual(order,facit)

        nodes = [bnvar.Variable(i,2) for i in range(1)]
        g = bn.BayesianNetwork(nodes)
        g = bn.BayesianNetwork(data)
        g.set_nodes(nodes)
        g.set_data(data)
        g.add_edges()

        order =  g.create_simulation_order()
        facit = [0]
        self.assertEqual(order,facit)


if __name__ == '__main__':
    unittest.main()
    
