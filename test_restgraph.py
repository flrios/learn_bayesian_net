import unittest
from chinese_rest import * 
from rest_graph import * 
from mcmc import  *
import numpy as np
import bayesian_network as bn
import networkx as nx
from ploting import *
import random
# THIS IS ESSENTIAL FOR THE TESTS
random.seed(1)
np.random.seed(1)

class TestRestGraph(unittest.TestCase):
    def setUp(self):
        data5 = np.array([[1,1,0,1,0],
                          [0,0,1,1,0],
                          [1,0,1,1,0]])

        self.r = Restaurant(3,5,0)
        self.r.assign_persons()
        #self.r.shuffle_tables()
    @unittest.skip("No real test")        
    def test_create_network(self):
        data = np.array([[1,1,0,1,0],
                          [0,0,1,1,0],
                          [1,0,1,1,0]])

        r = Restaurant(3,5,0)
        r.assign_persons()
        #print r
        #self.r.shuffle_tables()
        rg = RestGraph(r, 1,1,data)
        rg.set_edge_prob() 
        rg.create_network()
        #print rg.network

    @unittest.skip("No real test")
    def test_prob_zg(self):
        n = 3
        alpha = 1
        beta = [1,1]
        r = Restaurant(alpha,n,0)
        r.set_state([[1,2],[0]])

        nodes = [bnvar.Variable(i,2) for i in range(n)]
        net = bn.BayesianNetwork()       
        net.set_nodes(nodes)
        rg = RestGraph(r,beta[0],beta[1])
        rg.set_network(net)

    def test_get_table_graph(self):
        n = 5
        r = Restaurant(1,n,0)

        ########## Paper
        r.set_state([[0],[1],[2,3],[4]])
        nodes = [bnvar.Variable(i,2) for i in range(n)]
        net = bn.BayesianNetwork()
        net.set_nodes(nodes)
        net.add_edges([(1,3),(3,4),(2,4)])
        rg = RestGraph(r,1,1)
        rg.set_network(net)
        #print rg.get_table_graph()
        self.assertEqual(rg.get_table_graph().edges(), [(1,2),(2,3)])

        #########
        n = 5
        r = Restaurant(1,n,0)        
        nodes = [bnvar.Variable(i,2) for i in range(n)]
        r.set_state([[0],[1],[2],[3],[4]])
        net = bn.BayesianNetwork()
        net.set_nodes(nodes)
        net.add_edges([(0,1),(2,3),(3,4)])

        rg = RestGraph(r,1,1)
        rg.set_network(net)
        #print rg.get_table_graph()
        self.assertEqual(rg.get_table_graph().edges(), [(0,1),(2,3),(3,4)])
        # self.assertEqual(rg.get_table_graph(), [1,3])



    @unittest.skip("No real test")
    def test_order(self):
        data = np.array([[1,1,0,1,0],
                          [0,0,1,1,0],
                          [1,0,1,1,0]])

        r = Restaurant(5,5,0)
        rg = RestGraph(r, 1,1)
        r.set_z([0,1,2,3,1])
        rg.set_sigma([3,4,1,2,0])
        rg.set_table_order([0,1,2,3])
        #print "z "+str(rg.rest.z)
        #print str(rg.get_tables())
        #print rg.get_persons_table_order(0)
        #print rg.get_persons_table_order(1)
        #print rg.get_persons_table_order(2)
        #print rg.get_persons_table_order(3)
        #print rg.get_persons_table_order(4)
        ##print "m "+str(rg.rest.m)
        ##print str(rg.rest.get_tables())
        ##print "sigma: " +str(rg.person_sigma)
        i = 1
        inv = rg.get_sigma_inv(i)
        ##print "delete person with sigma(p) = "+str(i)+" dvs crp id= "+str(inv)
        rg.remove_person(i)        
        ##print "z "+str(rg.rest.z)
        ##print "m "+str(rg.rest.m)
        ##print "sigma: " +str(rg.person_sigma)
        ##print "add with sigma(p) = "+str(i)+" (OLD crp id= "+str(inv)
        rg.add_person(i) 
        ##print "sigma: " +str(rg.person_sigma)
        ##print "z "+str(rg.rest.z)
        ##print "m "+str(rg.rest.m)
        ##print str(rg.rest.get_tables())
        #print str(rg.get_tables())

        order = [0,3,2,1]
        #print "set order"
        ##print order
        rg.set_table_order(order)
        #for i in range(len(order)):
            #print "table "+str(i)+" "+ str(rg.get_tables()[i])
        #print rg.table_order
        #print str(rg.get_tables())
        #print "move table 3 to pos 1"
        rg.move_table_to_pos(3,1)
        #print str(rg.get_tables())
        #print rg.table_order
        #print "move table 0 to pos 3"
        rg.move_table_to_pos(0,3)
        #print str(rg.get_tables())
        #print rg.table_order

        #for i in range(len(order)):
            #print "table "+str(i)+" "+ str(rg.get_tables()[i])


        #print rg.get_persons_table_order(0)
        #print rg.get_persons_table_order(1)
        #print rg.get_persons_table_order(2)
        #print rg.get_persons_table_order(3)
        #print rg.get_persons_table_order(4)
    @unittest.skip("No real test")
    def test_shuffle(self):
        data = np.array([[1,1,0,1,0],
                          [0,0,1,1,0],
                          [1,0,1,1,0]])

        r = Restaurant(5,5,0)
        r.assign_persons()
        self.r.shuffle_tables()
        #print r
        #self.r.shuffle_tables()
        rg = RestGraph(r, 1,1,data)
        rg.set_edge_prob() 
        rg.create_network()
        #print rg.network
        for i in range(20):
            rg.shuffle()
            #print rg.rest
            #print rg.network
            #print

    @unittest.skip("No real test")
    def test_create_edges_from_to(self):
        r = self.r.copy()
        ##print r
        n = r.network.get_networkx()
        ##print nx.to_numpy_matrix(n,range(r.no_persons))
        r.create_edges_from_person(0,1) 
        r.create_edges_to_person(2,1)
        n = r.network.get_networkx()
        ##print nx.to_numpy_matrix(n,range(r.no_persons))
    @unittest.skip("No real test")
    def test_delete_edges_from_to(self):
        r = self.r.copy()
        ##print r
        n = r.network.get_networkx()
        ##print r.nu
        ##print nx.to_numpy_matrix(n,range(r.no_persons))
        r._edges_from_person(0,1) 
        r.create_edges_to_person(2,1)
        n = r.network.get_networkx()
        ##print nx.to_numpy_matrix(n,range(r.no_persons))

    def test_get_bad_edges(self):
        n = 8
        r = Restaurant(1,n,0)
        r.set_state([[6,7],[0,1,2],[5],[3,4]])

        nodes = [bnvar.Variable(i,2) for i in range(n)]
        net = bn.BayesianNetwork()
        net.set_nodes(nodes)
        net.add_edges([(0,5),(1,2),(3,5)])
        rg = RestGraph(r,1,1)
        rg.set_network(net)

        self.assertTrue( set(rg.get_bad_edges(r.persons)) == set([(3,5),(1,2)]) )

        edges = rg.get_bad_edges(r.persons)
        
        rg.network.reverse_edges(edges)

        #print rg
    @unittest.skip("No real test")
    def test_shuffle_restaurant_alt2(self):
        r = self.r.copy()
        #print r
        n = r.network.get_networkx() 
        #print nx.to_numpy_matrix(n,range(5))

        for i in range(105):
            #print str(i)+" - SHUFFLE alt2"
            r.shuffle_restaurant("alt2")
            #print r
            n = r.network.get_networkx() 
            #print nx.to_numpy_matrix(n,range(5))
            #print r.prob()
    @unittest.skip("No real test")
    def test_cond_graph_prob(self):
        r = self.r.copy()

        n = r.network.get_networkx() 
        #print r
        #print nx.to_numpy_matrix(n,range(5))
        #print "Edges between tables"
        #print r.get_edges_between_tables()
        #print "Score: " +str(r.prob())
        #print         

        for i in range(100):
            r = self.r.copy()
            #print str(i)+" --- SHUFFLE alt2 ---"
            r.shuffle_restaurant("alt2")
            n = r.network.get_networkx() 
            #print r
            #print "#Print edges: "+str(r.network.edges)
            ##print nx.to_numpy_matrix(n,range(5))
            #print "Edges between tables"
            #print r.get_edges_between_tables()        
            #print "Score: " +str(r.prob())
            #print "\n\n"      
        #print r.nu
